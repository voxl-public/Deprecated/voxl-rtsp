# VOXL-RTSP

A simple application to stream video from a VOXL camera using RTSP and, optionally, store video locally to a mp4 file.

## Build
Build the application using the **voxl-docker** environment described here: https://docs.modalai.com/build-environments/.

1. Run VOXL Docker emulator image from *docker_home* directory.
```
$ cd docker_home
$ voxl-docker
```
or
```
$ cd docker_home
$ voxl-docker -i voxl-emulator
```

2. Call *make* to build application and exit docker environment.
```
$ make
$ exit
```

3. Push executable to VOXL target using ADB.
```
$ adb push voxl-rtsp /home/root
```

## Run
Run the video streaming application from VOXL device and view using a RTSP viewer such as VLC or QuickTime player.

1. Run streaming server on target.
Run with default configuration: camera ID = 0, 640x480 at 30 fps, h264 encoding. Look for the RTSP server URL in application output (*rtsp://device_ip:port/live*).
```
$ ./voxl-rtsp
voxl-rtsp version 1.0.3
Connected
Started camera 0
Created session
Video track created
Updated camera parameters
AE Mode: 1, Exposure: 0us, Gain: 0us, AWB Mode: 1
rtsp://192.168.10.218:8900/live
Camera Control FIFO starting: /dev/camera0_control
Started session
Ctrl-c or 'kill 3936' to end
[112.850173] fps 29.95, size 90244
...
(user pressed Ctrl-c)
...
Stopped session
Video track deleted
Session deleted
Stopped Camera
Disconnected
voxl-rtsp exiting

Summary:
Camera #         : 0
Resolution       : 640x480
Format           : h264
FPS Desired      : 30.00
FPS Actual       : 29.96
Duration (s)     : 2.508
Frame Count      : 66
Total Size (MB)  : 0.28
Bandwidth (Mbps) : 0.89
Bytes per Frame  : 4417
```

Use -h option to view the help menu provided below.
```
$ ./voxl-rtsp -h
voxl-rtsp version 1.0.3
Options:
-i                Query and print camera information
-w <mode>         Set white balance mode (off,auto,incandescent,fluorescent,warm-fluorescent,daylight,cloudy-daylight,twilight,shade)
-e <exposure_us>  Set manual exposure (in microseconds). Make sure to set manual gain as well
-g <gain>         Set manual camera gain (camera-specific). Make sure to set manual exposure as well
-c <number>       Camera number (0 by default)
-f <format>       Video output format selection (h264 (default), h265, jpeg, yuv, yuv-padded, raw8, raw10, raw12, blob)
-s <resolution>   Image resolution (640x480 by default)
-r <framerate>    Stream frame rate (30 by default)
-b <bitrate>      Stream bit rate for h264/h265 (1M by default)
-m <fifo_name>    Write raw data to a specified fifo (will be created)
-o <filename>     Save video to filename (No save to file by default)
-p <port>         Select port for RTSP server (8900 by default)
-a <angle>        Set rotation angle for the image (0 or 180 only)
-t <time>         Session timeout in seconds
-d <N>            Dump every N'th frame to disk. 0 to disable (default)
-h                Show help
```

2. Open server's RTSP URL in VLC or QuickTime player: *rtsp://device_ip:port/live*

## Gap removal in YUV frames
YUV frame output can be achieved using `-f yuv` or `-f yuv-padded`. YUV image consists of two planes (Y and UV) and there are requirements on memory alignment for those planes. In particual, width and height need to be a multiple of 32. If the height is not a multiple of 32 (720, 1080, etc), the beginning of UV plane is offset to a boundary that is aligned with 32 rows (736,1088, etc). This means that YUV image coming from ISP is not contiguous and contains a gap. For convenience, this application will remove the gap between Y and UV plane if `-f yuv` mode is used and will return the original padded image if `-f yuv-padded` mode is used.

## Additional Examples

### Run with custom configartion.
```
$ ./voxl-rtsp -c 0 -s 1280x720 -r 30 -f yuv
voxl-rtsp version 1.0.3
Setting camera number to: 0
Setting resolution: 1280x720
Setting frame rate (default 30 fps): 30
Setting output format to: yuv
Connected
Started camera 0
Created session
Video track created
Updated camera parameters
AE Mode: 1, Exposure: 0us, Gain: 0us, AWB Mode: 1
Camera Control FIFO starting: /dev/camera0_control
Started session
Ctrl-c or 'kill 4060' to end
[418.941767] fps 30.00, size 1382400
...
(user pressed Ctrl-c)
...
Stopped session
Video track deleted
Session deleted
Stopped Camera
Disconnected
voxl-rtsp exiting

Summary:
Camera #         : 0
Resolution       : 1280x720
Format           : yuv
FPS Desired      : 30.00
FPS Actual       : 30.00
Duration (s)     : 4.622
Frame Count      : 134
Total Size (MB)  : 176.66
Bandwidth (Mbps) : 305.78
Bytes per Frame  : 1382400
```

### Get camera information
```
./voxl-rtsp -c 0 -i
voxl-rtsp version 1.0.3
Setting camera number to: 0
Connected
Started camera 0
Created session
Video track created
Querying Camera Information...
ANDROID_SCALER_AVAILABLE_RAW_SIZES:
	4056x3040, 3840x2160, 1920x1080, 1280x720,

ANDROID_SCALER_AVAILABLE_PROCESSED_SIZES:
	4056x3040, 4000x3000, 3040x3040, 3016x3016, 3840x2160, 3648x2736, 3264x2448, 3200x2400,
	2976x2976, 3044x1720, 2704x2028, 2704x1520, 2592x1944, 2688x1512, 2028x1144, 2160x2160,
	1920x2160, 1920x1920, 1880x1880, 2048x1536, 1920x1440, 1920x1080, 1600x1600, 1600x1200,
	1520x1520, 1440x1080, 1080x1080, 1280x 960, 1340x 760, 1280x 768, 1280x 720, 1200x1200,
	1280x 640, 1280x 480, 1040x 780, 1024x 768,  960x 960,  720x 720,  800x 600,  960x 720,
	 848x 480,  858x 480,  864x 480,  800x 480,  720x 480,  640x 480,  640x 240,  640x 360,
	 480x 640,  480x 480,  480x 360,  480x 320,  432x 240,  352x 288,  320x 240,

ANDROID_SENSOR_INFO_SENSITIVITY_RANGE
	min = 50
	max = 17804

ANDROID_SENSOR_MAX_ANALOG_SENSITIVITY
	1113

ANDROID_SENSOR_INFO_EXPOSURE_TIME_RANGE
	min = 7521ns
	max = 325013920ns
```

Note that querying camera information does not work for TOF camera.

## Gain and Exposure
Automatic exposure / gain is enabled by default, but manual exposure and gain can be set using -e and -g options when application is started or in real time using the control pipe. Voxl-rtsp creates a pipe for accepting control messages, for example it prints the following when started:
```
Camera Control FIFO starting: /dev/camera0_control
```

Exposure and gain can be controlled like so (run command on VOXL):
```
echo "exposure_us=30000" > /dev/camera0_control
echo "gain=500" > /dev/camera0_control
```

To use auto exposure / gain, set either exposure or gain to 0:
```
echo "exposure_us=0" > /dev/camera0_control
or
echo "gain=0" > /dev/camera0_control
```

Additional Information:
- `-i` option provides the min and max exposure (in microseconds), min/max analog sensitivity (gain), min/max sensitivity (gain).
- if manual exposure is set to exceed 1/fps, then the fps will be lowered to achieve the requested exposure.
- if gain setting is below `MAX_ANALOG_SENSITIVITY`, then camera's analog gain will be used and digital gain will be set to 1.0. This provides the best performance, since gain is controlled before sampling the pixel values.
- if gain setting is above `MAX_ANALOG_SENSITIVITY`, the camera's analog gain will be maxed out and digital gain will be applied to achieve additional gain so that total gain is equal to the requested gain.

## Real-time bitrate update
Bitrate for h264 / h265 can be updated in real-time, similar to exposure and gain updates:
```
echo "bitrate=1000000" > /dev/camera0_control
```

## Streaming Multiple Cameras
- up to two cameras can be streamed concurrently in YUV / h264 / h265 mode
- simply start two voxl-rtsp applications their appropriate parameters (selecting different cameras, etc)
- both streams are using h264 or h265 (encoded video), RTSP server will be automatically enabled, so the two applications need to use different ports (-p option)
- three concurrent streams are possible, however:
   - at least one camera needs to use RAW mode
   - cameras using RAW mode need to be started first
   - user needs to know which RAW mode to select (RAW8 or RAW10, most likely RAW10, depending on the camera)
