v1.0.3
- add option to update bitrate in real-time
- add camera control input for exposure and gain via fifo
- add back -m parameter for writing to a fifo, but it now requires the fifo name as argument; make yuv contiguous if needed, added option yuv-padded if this is not desired
- print max total sensitivity in camera info
- print when a frame is saved to disk
- add option to print camera info (-i)
- add option to dump every nth frame in any format (-d)
- add option to specify auto white balance mode with -w <mode>
- add option to write to fifo, stdout
- add timeout option (-t) and calculate and print stats at the end
- add option to set rotation angle of 0 or 180 (rotation happens in ISP)
- add option to specify output format including h264 / h265
- add option to provide manual gain and exposure values (-g <gain>, -e <exposure_us>)
- print app version, added param for rtsp port number (-p), disable low power mode for fps > 30

v1.0.2
- Original Release
